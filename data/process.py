#!/usr/bin/python

import json

input_file = 'swipe.json'
output_file = 'processed_swipe.json'

with open(input_file) as fp:
    lines = fp.readlines()

first_json = json.loads(lines[0])
final_swipes = []

for this_swipe in first_json['swipes']:
    if this_swipe['isRejected'] == False:
        final_swipes.append(this_swipe)

if len(first_json) == 1:
    print('Only one json file present. Quitting')
    quit()

for this_json_line in lines[1:]:
    this_json = json.loads(this_json_line)

    for this_swipe in this_json['swipes']:
        if this_swipe['isRejected'] == False:
            final_swipes.append(this_swipe)

final_json = first_json
final_json['swipes'] = final_swipes
final_json_string = json.dumps(final_json, indent = 4)

with open(output_file, 'w') as fp:
    fp.write(final_json_string)

print('Processed swipe json successfully')