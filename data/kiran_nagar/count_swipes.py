#!/usr/bin/python

import json

input_file = 'processed_swipe.json'

with open(input_file) as fp:
    json_object = json.load(fp)

print('Count of swipes in the json is: ' + str(len(json_object['swipes'])))