#!/usr/bin/python

import json

input_file = 'processed_swipe.json'
output_file = 'paragraph.txt'

with open(input_file) as fp:
    json_object = json.load(fp)

result = ''

for this_swipe in json_object['swipes']:
    if this_swipe['isRejected'] == False:
        if this_swipe['replacedText'] != '':
            result += this_swipe['replacedText']
        else:
            result += this_swipe['text']
        result += '\n'

with open(output_file, 'w') as fp:
    fp.write(result)

print('Extracted paragraph successfully')