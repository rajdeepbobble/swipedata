#!/usr/bin/python

import json

input_file = 'processed_swipe.json'

with open(input_file) as fp:
    json_object = json.load(fp)

result = ''

total_swipes = len(json_object['swipes'])
accepted_swipes = 0

for this_swipe in json_object['swipes']:
    if this_swipe['replacedText'] == '':
        accepted_swipes += 1

accuracy = accepted_swipes / total_swipes * 100
print('Swipe accuracy is ' + str(accuracy))