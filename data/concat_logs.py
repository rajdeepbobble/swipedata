#!/usr/bin/python

import os

file_list = os.listdir()
directory_names = []
output_file_name = 'swipe.json'
output_fp = open(output_file_name, 'w')

for this_file in file_list:
    if os.path.isdir(this_file):
        directory_names.append(this_file)

for this_directory in directory_names:
    this_swipe_log_path = this_directory + '/swipe.json'
    with open(this_swipe_log_path) as fp:
        lines = fp.readlines()
        for line in lines:
            output_fp.write(line)

output_fp.close()
print('Successfully concatenated the log files into ' + output_file_name)